package jav;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;



public class searchVendorName {
	
	@Test
	public void test() {
		
		System.setProperty("webdriver.chrome.driver", "C:\\TestLeaf\\Selenium\\workspace\\Test03\\drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://acme-test.uipath.com/account/login");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		WebElement txtboxUserName = driver.findElementByXPath("//input[@id='email']");
		txtboxUserName.clear();
		txtboxUserName.sendKeys("asifrm786@gmail.com");
		WebElement txtboxPassword = driver.findElementByXPath("//input[@id='password']");
		txtboxPassword.clear();
		txtboxPassword.sendKeys("asif1234");
		driver.findElementByXPath("//button[@id='buttonLogin']").click();
		
		Actions act = new Actions(driver);
		WebElement lnkVendors = driver.findElementByXPath("//div[@id='dashmenu']/div[5]/button");
		act.moveToElement(lnkVendors).perform();
		driver.findElementByXPath("//a[text()='Search for Vendor']").click();
		
		WebElement txtboxVendorTaxID = driver.findElementByXPath("//input[@id='vendorTaxID']");
		txtboxVendorTaxID.clear();
		txtboxVendorTaxID.sendKeys("DE767565"); 
		driver.findElementByXPath("//button[@id='buttonSearch']").click();
		
		WebElement table = driver.findElementByXPath("//table[@class='table']");
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		List<WebElement> columns = rows.get(1).findElements(By.tagName("td"));
		System.out.println(columns.get(0).getText());
		
	}

}
